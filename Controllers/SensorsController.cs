using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Occupeye.Models;

namespace Occupeye.Controllers
{
    public class SensorsController : Controller
    {

        public IEnumerable<Sensors> _sensors { get; set; }

        private readonly occupeyedbContext _context;

        public SensorsController(occupeyedbContext context){
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> Index(){
        using (var client = new HttpClient())
        {
            client.BaseAddress = new Uri("http://localhost:5000/api/sensorsapi/");
            var responseTask = client.GetAsync("");
            responseTask.Wait();
            var result = responseTask.Result;
            if (result.IsSuccessStatusCode)
            {
                var readTask = result.Content.ReadAsAsync<List<Sensors>>();
                readTask.Wait();
                _sensors = readTask.Result;
            }else{
                return View("error no model found");
            }
        }
        return await Task.Run(() => View(_sensors));
        }

        [HttpPost]
        public async Task<IActionResult> Add(Sensors sensors){
            if (ModelState.IsValid)
            {
                
                await _context.Sensors.AddAsync(sensors);
                await _context.SaveChangesAsync();
                return await Task.Run(() => Redirect("Index"));
            }
            else
            {
                return View();
            }
        }
        
        [HttpGet]
        public async Task<IActionResult> Add(){
                    return await Task.Run(() => View());
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int? id){
		using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:5000/api/sensorsapi/");
                    var deleteTask = client.DeleteAsync(id.ToString());
                    deleteTask.Wait();

                    var result = deleteTask.Result;

                    return await Task.Run(() => RedirectToAction("Index"));
                }
        }
    }
}