using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Occupeye.Models;

namespace Occupeye.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SensorsApiController : ControllerBase
    {
        private readonly occupeyedbContext _context;

        public SensorsApiController(occupeyedbContext context){
            _context = context;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            Sensors model = await _context.Sensors.FirstOrDefaultAsync(m => m.Id == id);
            if (model == null)
            {
                return NotFound();
            }
            return Ok(model);
        } 

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            List<Sensors> model = await _context.Sensors.ToListAsync();
            if (model == null)
            {
                return NotFound();
            }
            return Ok(model);
        } 

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Sensors value)
        {
         return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }  
            Sensors delete = await _context.Sensors.FirstOrDefaultAsync(m => m.Id == id); 

                 _context.Sensors.Remove(delete);
                await _context.SaveChangesAsync();
         return Ok();
        }
    }
}