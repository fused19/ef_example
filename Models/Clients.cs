﻿using System;
using System.Collections.Generic;

namespace Occupeye.Models
{
    public partial class Clients
    {
        public Clients()
        {
            Sensors = new HashSet<Sensors>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Sensors> Sensors { get; set; }
    }
}
