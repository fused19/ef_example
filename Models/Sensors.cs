﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Occupeye.Models
{
    public partial class Sensors
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public int? ClientId { get; set; }
        [Required]
        public string Locale { get; set; }
        public bool Activate { get; set; }
        public bool? Data { get; set; }

        public Clients Client { get; set; }
    }
}
